//
//  test.c
//  adb
//
//  Copyright (c) 2012, Michael Sokolnicki
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met: 
//
//  1. Redistributions of source code must retain the above copyright notice, this
//     list of conditions and the following disclaimer. 
//  2. Redistributions in binary form must reproduce the above copyright notice,
//     this list of conditions and the following disclaimer in the documentation
//     and/or other materials provided with the distribution. 
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "adb.h"
#include <stdio.h>
#include <stdlib.h>

#define TESTPATH "/tmp/db/testvar"

#define assert_success(expr,msg) do { if (expr<0) { printf("Error: %s\n", msg); \
                                    return expr; } } while(0)

typedef struct point {
    int x; int y; int z;
} point;

static int printVector(const point *vec)
{
    return printf("(%i,%i,%i)\n", vec->x, vec->y, vec->z);
}

static int testwrite(void)
{
    int ret, i;
    
    // Insertion example
    int count = 100;
    struct point vecs[100];
    
    for (i = 0; i < count; i++) {
        vecs[i].x = i;
        vecs[i].y = i + 1;
        vecs[i].z = i + 2;
    }

    adb_var var = {0};
    var.path    = TESTPATH;
    var.opmode  = ADB_WRMODE;
    var.start   = ADB_NEWBLOCK;
    var.type.storage = ADB_STAMPED;
    var.type.size    = sizeof(struct point);
    
    ret = adb_open(&var);
    assert_success(ret, "adb_open");
    for (i = 0; i < count; i++) {
        ret = adb_insert(&var, adb_now(), &vecs[i]);
        assert_success(ret, "adb_insert");
    }
    ret = adb_close(&var);
    assert_success(ret, "adb_close");
    return 0;
}

static int testquery(void)
{
    int ret;
    
    // Query example
    const point *vec;
    adb_time end_ts;
    adb_var var = {0};
    var.path    = TESTPATH;
    var.opmode  = ADB_RDMODE;
    var.start   = adb_now() - 3 * ADB_WEEK;
    end_ts      = adb_now() + 2 * ADB_MIN;
    
    ret = adb_open(&var);
    assert_success(ret, "adb_open");
    do {
        vec = adb_value(&var);
        printVector(vec);
        ret = adb_next(&var);
        assert_success(ret, "adb_next");
        if (ret == 1) break;
    } while (adb_key(&var) < end_ts);
    ret = adb_close(&var);
    assert_success(ret, "adb_close");
    return 0;
}

static int testclean(void)
{
    // Compacting example
    adb_time end_ts;
    adb_var var = {0};
    var.path    = TESTPATH;
    var.opmode  = ADB_CLMODE;
    var.start   = ADB_BEGINNING;
    end_ts      = adb_now() - 12 * ADB_HOUR;
    
    adb_open(&var);
    do {
        adb_next(&var);
        adb_remove(&var);
        adb_remove(&var);
        adb_remove(&var);
    } while (adb_key(&var) < end_ts);
    adb_close(&var);
    return 0;
}

int main(int argc, char **argv)
{
    printf("Andrew DB test starting...\n");
    
    // Prep
    int ret = system("mkdir -p " TESTPATH);
    if (ret < 0) {
        printf("Can't create folders\n");
        return -1;
    }
    
    // Insertion test
    testwrite();

    // Query test
    testquery();
    
    // Clean test
    //testclean();
    return 0;
}
