module Main where
import Monad
import System.Environment
import Text.ParserCombinators.Parsec
import Adb.TimeExpr
import Adb.DataExpr
import Adb.Variables
import Adb.Interface

main :: IO ()
main = getArgs >>= putStrLn . readExpr . (!! 0)

readExpr :: String −> String
readExpr input = case parse parseCommand "adb" input of
    Left err −> "No match: " ++ show err
    Right val −> val

data FilterExpr = Filter Expr | None

data TransformExpr = Map Expr
                   | Reduce Expr
                   | None

data Command = Insert Name (Maybe TimeExpr) Value
             | Delete Name (Maybe TimeExpr)
             | Query Name (Maybe TimeExpr) FilterExpr TransformExpr


parseSlice :: Parser TimeExpr
parseSlice = char '(' >> parseTimeExpr >> char ')'

parseNameSlice :: Parse (Name, Maybe TimeExpr)
parseNameSlice = do
                    name <- parseName
                    slice <- ((try parseSlice) >>= Just) <|> None
                    return (name, slice)

parseInsert :: Parser Command
parseInsert = do
                (name, slice) <- parseNameSlice
                string "<-"
                value <- parseDataExpr
                return $ Insert name slice value

parseDelete :: Parser Command
parseDelete = do
                string "delete"
                (name, slice) <- parseNameSlice
                return $ Delete name slice

parseQuery :: Parser Command
parseQuery = do
                (name, slice) <- parseNameSlice
                dfilter <- try (string "where" >> parseDataExpr >>= Filter) <|> None
                tranform <- (try (string "map" >> parseDataExpr >>= Map))
                        <|> (string "reduce" >> parseDataExpr >>= Reduce)
                        <|> None
                return $ Query name slice dfilter tranform

parseCommand :: Parser Command
parseCommand = parseInsert
           <|> parseDelete
           <|> parseQuery


showSlice :: TimeExpr -> String
showSlice tv = "(" ++ showTimeExpr tv ++ ")"

showNameSlice :: Name -> Maybe TimeExpr -> String
showNameSlice n s = showName n ++ case s of Just tv -> showSlice tv
                                            Nothing -> ""
showCommand :: Command -> String
showCommand (Insert n s v) = showNameSlice n s
                          ++ " <- "
                          ++ showDataExpr v
showCommand (Delete n s) = "delete "
                        ++ showNameSlice n s
showCommand (Query n s f t) = showNameSlice n s
                           ++ case f of Filter e -> "where " ++ showDataExpr e
                                        None -> ""
                           ++ case t of Map e -> "map " ++ showDataExpr e
                                        Reduce e -> "reduce " ++ showDataExpr e
                                        None -> ""

