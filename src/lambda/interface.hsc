{-# LANGUAGE ForeignFunctionInterface #-}

module Core.Interface where

import Foreign
import Foreign.C

#include <adb.h>

-- Defines
us :: CInt64
us = 1
ms = 1000 * us
sec = 1000 * ms
min = 60 * sec
hour = 60 * min
day = 24 * hour
week = 7 * day

invalTime = minBound :: CInt64
beginning = minBound :: CInt64 + 1
endOfTime = maxBound :: CInt64

oldBlock = 0
newBlock = 1

stamped = 0
slotted g = g
cstring = -1

-- Opmode
newtype Opmode = Opmode (CInt) deriving (Eq, Show)

#{enum Opmode, Opmode,
    writeMode   = ADB_WRMODE,
    readMode    = ADB_RDMODE,
    cleanMode   = ADB_CLMODE
 }

-- Time
type Time = CIntMax

data Type = Type
type TypeHandle = Ptr Type

data Var = Var
type VarHandle = Ptr Var


-- Functions
foreign import ccall "adb_now" now :: IO (Time)

foreign import ccall "adb_open" open :: VarHandle -> IO (CInt)

foreign import ccall "adb_close" close :: VarHandle -> IO (CInt)

foreign import ccall "adb_key" key :: VarHandle -> IO (Time)

foreign import ccall "adb_value" value :: VarHandle -> IO (CVoidPtr)

foreign import ccall "adb_insert" insert :: VarHandlr -> Time -> CVoidPtr -> IO (CInt)

foreign import ccall "adb_remove" remove :: VarHandle -> IO (CInt)

foreign import ccall "adb_prev" prev :: VarHandle -> IO (CInt)

foreign import ccall "adb_next" next :: VarHandle -> IO (CInt)