module Main where
import Monad
import Data.List
import System.Environment
import Text.ParserCombinators.Parsec
import Foreign.C


main :: IO ()
main = getArgs >>= putStrLn . readExpr . (!! 0)

readExpr :: String -> String
readExpr input = case parse parseTimeExpr "adb" input of
    Left err -> "No match: " ++ show err
    Right val -> showTimeExpr val

newtype Time = Time CIntMax
newtype Span = Span CIntMax

type Name = [String]

data TimeVal = Instant Time
             | Range Time Time
             | GRange Time Time Span

--(+) :: Time -> Span -> Time
--Time t + Span s = Time (t + s)

--(-) :: Time -> Span -> Time
--Time t - Span s = Time (t - s)

--(<>) :: Time -> Time -> Span
--Time t1 <> Time t2 = Span (t2-t1)

--parseNow :: Parser Time
--parseNow = string "now" >> return Time 5

--parseBeg :: Parser Time
--parseBeg = string "beg" >> return Time 0

--parseEnd :: Parser Time
--parseEnd = string "end" >> return Time 10

parseTimeLiteral :: Parser Time
parseTimeLiteral = liftM (Time . read) $ many1 digit

parseTime :: Parser Time
parseTime = parseTimeLiteral -- <|> parseNow <|> parseBeg <|> parseEnd

parseSpan :: Parser Span
parseSpan = liftM (Span . read) $ many1 digit

parseInstant :: Parser TimeVal
parseInstant = liftM Instant $ parseTime

parseRange :: Parser TimeVal
parseRange = do
                beg <- parseTime
                end <- (spaces >> string "->" >> spaces >> parseTime)
                return $ Range beg end

parseGRange :: Parser TimeVal
parseGRange = do
                beg <- parseTime
                grain <- (spaces >> string "--" >> spaces >> parseSpan)
                end <- (spaces >> string "->" >> spaces >> parseTime)
                return $ GRange beg end grain

parseTimeVal :: Parser TimeVal
parseTimeVal = (try parseGRange)
           <|> (try parseRange)
           <|> parseInstant

parseTimeExpr :: Parser [TimeVal]
parseTimeExpr = sepBy parseTimeVal (spaces >> char ',' >> spaces)


showTime :: Time -> String
showTime (Time t) = "Time " ++ show t

showSpan :: Span -> String
showSpan (Span s) = "Span " ++ show s

showTimeVal :: TimeVal -> String
showTimeVal (Instant i) = showTime i
showTimeVal (Range beg end) = "Range (" ++ (showTime beg) ++ " " ++ (showTime end) ++ ")"
showTimeVal (GRange beg end g) = "GRange (" ++ (showTime beg) ++ " " 
                              ++ (showTime end) ++ " " ++ (showSpan g) ++ ")"

showTimeExpr :: [TimeVal] -> String
showTimeExpr = unwords . intersperse ", " . map showTimeVal

{-
time_expression := time_expression ',' time_expression
                |  time_val '->' time_val
                |  time_val '--' span '->' time_val
                |  time_val '+' span
                |  time_val '-' span

span := span '+' span
     |  span '-' span
     |  integer time_unit

time_unit := 'us'
          |  'ms'
          |  's'
          |  'm'
          |  'h'
          |  'd'
          |  'w'
time_val := "(\d{4})/(\d{2})/(\d{2})-(\d{2}):(\d{2}):(\d{2})(?:.(\d+))(?:([+-])(\d{4})Z"
         |  'now'
         |  'beg'
         |  'end'
-}

