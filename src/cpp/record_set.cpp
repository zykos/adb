//
//  record_set.cpp
//  adb
//
//  Copyright (c) 2012, Michael Sokolnicki
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met: 
//
//  1. Redistributions of source code must retain the above copyright notice, this
//     list of conditions and the following disclaimer. 
//  2. Redistributions in binary form must reproduce the above copyright notice,
//     this list of conditions and the following disclaimer in the documentation
//     and/or other materials provided with the distribution. 
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "record_set.hpp"

#include <algorithm>

#include "record.hpp"

namespace adb {

RecordSet *
RecordSet::openFile(const std::string &varpath, Time base_ts,
        StorageType storage, size_t payload_size)
{
    error_ = kOk;
    File *file = File::openIn(varpath, std::string(base_ts));
    if (!file) {
        error_ = File::error();
        return NULL;
    }

    RecordSet *rs;
    switch (storage) {
        case kSlotted:
            rs = SlottedRecordSet::fromFile(*file, payload_size, base_ts);
            break;
        case kStamped:
            rs = StampedRecordSet::fromFile(*file, payload_size, base_ts);
            break;
        case kGeneric: /* TODO */
        default: error_ = kErrorStorageInvalid; return NULL;
    }
    if (!rs) delete file;

    return rs;
}

Error RecordSet::error_ = kOk;

/* OrderedRecordSet*/


std::list<Record*> *
OrderedRecordSet::between(Time from, Time to, size_t count)
{
    std::list<Record*> *results = new std::list<Record*>;
    for (off_t i = tsToIndex(from), n = 0;
         i < (signed)size() && n < (signed)count; i++, n++) {
        Record *rec = readRecordAt(i);
        if (rec->timestamp() > to) break;
        results->push_back(rec);
    }
    return results;
}

Record &
OrderedRecordSet::operator[](off_t i)
{
    // if (records.find(i) == records.end()) {
    //     // Read from disk
    //     records[i] = readRecordAt(i);
    // }
    // return records[i];
    return *readRecordAt(i);
}

OrderedRecordSet::iterator
OrderedRecordSet::begin()
{
    return iterator(*this, 0);
}

OrderedRecordSet::iterator
OrderedRecordSet::end()
{
    return iterator(*this, size()-1);
}

/* GenericRecordSet */

// TODO

/* SlottedRecordSet */

SlottedRecordSet::SlottedRecordSet(const File &file, size_t payload_size, 
        Time base_ts)
    : OrderedRecordSet(file, payload_size, base_ts)
{
    nrecords_ = (file_.size() - sizeof(interval_)) / payload_size;
}

Time
SlottedRecordSet::insert(const void *payload)
{
    error_ = kOk;
    // Get insertion slot
    off_t i = nrecords_++;

    // Write the record
    ssize_t ret = file_.lockedWrite(payloadOffset(i), payload_size(), payload);
    if (ret < 0) {
        error_ = file_.error();
        return 0;
    }
    return indexToTs(i);
}

Time
SlottedRecordSet::insert(Time ts, const void *payload)
{
    error_ = kOk;
    // Compute insertion slot
    off_t i = tsToIndex(ts);

    // If ts in the past, return error
    if (i < (signed)nrecords_) {
        error_ = kErrorRecordTimePast;
        return 0;
    }

    // Lock for writing the region that will be added
    size_t nbytes = payload_size() * (i + 1 - size());
    ssize_t ret = file_.lockExclusive(payloadOffset(nrecords_), nbytes);
    if (ret < 0) {
        error_ = file_.error();
        return 0;
    }
     
    // If ts is not the next one, pad with the same value
    if (i > (signed)nrecords_) {
        void *last_payload = new char[payload_size()];
        ret = file_.lockedRead(payloadOffset(size()-1),
                payload_size(), last_payload);
        if (ret < 0) goto error;
        for (; (signed)nrecords_ < i; nrecords_++) {
            ret = file_.lockedWrite(payloadOffset(nrecords_),
                    payload_size(), last_payload);
            if (ret < 0) goto error;
        }
    }

    // Write the record
    ret = file_.plainWrite(payloadOffset(i), payload_size(), payload);
    if (ret < 0) goto error;
    file_.unlock(payloadOffset(size()), nbytes);
    nrecords_ = i;
    return indexToTs(i);
error:
    error_ = file_.error();
    file_.unlock(payloadOffset(size()), nbytes);
    return 0;
}

Record *
SlottedRecordSet::readRecordAt(off_t i)
{
    error_ = kOk;
    char *payload = new char[payload_size()];
    ssize_t ret = file_.lockedRead(payloadOffset(i), payload_size(), payload);
    if (ret < 0) {
        error_ = file_.error();
        delete [] payload;
        return NULL;
    }
    return new Record(indexToTs(i), payload, payload_size());
}

off_t
SlottedRecordSet::tsToIndex(Time ts) const
{
    if (ts < base_ts()) return 0;
    return (ts - base_ts()) / interval_;
}

SlottedRecordSet *
SlottedRecordSet::fromFile(const File &file, size_t payload_size, Time base_ts)
{
    error_ = kOk;
    SlottedRecordSet *rs = new SlottedRecordSet(file, payload_size, base_ts);
    Time interval;
    ssize_t ret = rs->file_.lockedRead(0, sizeof(interval), &interval); 
    if (ret < 0) {
        error_ = rs->file_.error();
        delete rs;
        return NULL;    
    }
    rs->interval_ = interval;
    return rs;
}

SlottedRecordSet *
SlottedRecordSet::create(const std::string &varpath, Time base_ts,
        size_t payload_size, Time interval)
{
    error_ = kOk;
    File *file = File::openIn(varpath, std::string(base_ts));
    if (!file) {
        error_ = File::error();
        return NULL;
    }
    ssize_t ret = file->lockedWrite(0, sizeof(interval), &interval); 
    if (ret < 0) {
        error_ = file->error();
        delete file;
        return NULL;    
    }
    return SlottedRecordSet::fromFile(*file, payload_size, base_ts);
}

/* StampedRecordSet */

StampedRecordSet::StampedRecordSet(const File &file, size_t payload_size, Time base_ts)
    : OrderedRecordSet(file, payload_size, base_ts)
{
    
}

inline Time
StampedRecordSet::insert(const void *payload)
{
    return insert(Time::now(), payload);
}

Time
StampedRecordSet::insert(Time ts, const void *payload)
{
    error_ = kOk;
    off_t i = size();

    ssize_t ret = file_.lockExclusive(tsOffset(i), sizeof(ts) + payload_size());
    if (ret < 0) {
        error_ = file_.error();
        return 0;
    }
    ret = file_.plainWrite(sizeof(ts), &ts);
    if (ret < (int)sizeof(ts))
        error_ = file_.error();
    ret = file_.plainWrite(payload_size(), payload);
    if (ret < (int)sizeof(ts))
        error_ = file_.error();
    ret = file_.unlock(tsOffset(i), sizeof(ts) + payload_size());
    if (error_ != kOk) return 0;
    if (ret < 0) {
        error_ = file_.error();
        return 0;
    }
    times_.push_back(ts);
    return ts;
}

Record *
StampedRecordSet::readRecordAt(off_t i)
{
    error_ = kOk;
    Time ts;
    ssize_t ret = file_.lockedRead(tsOffset(i), sizeof(ts), &ts);
    if (ret < 0) {
        error_ = file_.error();
        return NULL;
    }

    char *payload = new char[payload_size()];
    ret = file_.lockedRead(payload_size(), payload);
    if (ret < 0) {
        error_ = file_.error();
        delete [] payload;
        return NULL;
    }
    return new Record(ts, payload, payload_size());
}

off_t
StampedRecordSet::tsToIndex(Time ts) const
{
    std::vector<Time>::const_iterator it;
    it = lower_bound(times_.begin(), times_.end(), ts);
    if (it == times_.end()) return 0;
    return it - times_.begin();
}

StampedRecordSet *
StampedRecordSet::fromFile(File &file, size_t payload_size, Time base_ts)
{
    error_ = kOk;
    StampedRecordSet *rs = new StampedRecordSet(file, payload_size, base_ts);

    // Cache all timestamps from the file
    size_t nrecords = file.size() / (sizeof(Time) + payload_size);
    rs->times_.reserve(nrecords);
    ssize_t ret = rs->file_.lockShared();
    if (ret < 0) {
        error_ = rs->file_.error();
        delete rs;
        return NULL;
    } 
    for (unsigned int i = 0; i < nrecords; i++) {
        Time ts;
        ret = rs->file_.plainRead(rs->tsOffset(i), sizeof(ts), &ts);
        if (ret < (int)sizeof(ts)) {
            error_ = rs->file_.error();
            break;
        }
        rs->times_.push_back(ts);
    }
    ret = rs->file_.unlock();
    if (error_ != kOk) {
        delete rs;
        return NULL;
    }
    if (ret < 0) {
        error_ = rs->file_.error();
        delete rs;
        return NULL;
    }

    return rs;
}

StampedRecordSet *
StampedRecordSet::create(const std::string &varpath, Time base_ts,
        size_t payload_size)
{
    error_ = kOk;
    File *file = File::openIn(varpath, std::string(base_ts));
    if (!file) {
        error_ = File::error();
        return NULL;
    }
    return StampedRecordSet::fromFile(*file, payload_size, base_ts);
}

} /* namespace adb */
