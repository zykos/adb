include_directories(${ADB_SOURCE_DIR}/src)
link_directories(${ADB_BINARY_DIR}/src)

# Build ADB library
add_library(adb SHARED database.cpp file.cpp variable.cpp record_set.cpp)