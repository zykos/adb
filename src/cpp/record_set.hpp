//
//  record_set.hpp
//  adb
//
//  Copyright (c) 2012, Michael Sokolnicki
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met: 
//
//  1. Redistributions of source code must retain the above copyright notice, this
//     list of conditions and the following disclaimer. 
//  2. Redistributions in binary form must reproduce the above copyright notice,
//     this list of conditions and the following disclaimer in the documentation
//     and/or other materials provided with the distribution. 
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include <list>
#include <vector>

#include "file.hpp"
#include "time.hpp"
#include "types.hpp"

#ifndef ADB_RECORD_SET_HPP_
#define ADB_RECORD_SET_HPP_

namespace adb {

class RecordSet {
public:
    virtual ~RecordSet() {};

    /* Data access */
    virtual Record &at(Time ts) = 0;
    virtual std::list<Record*> *between(Time from, Time to, size_t count) = 0;
    virtual Time insert(const void *value) = 0;
    virtual Time insert(Time ts, const void *value) = 0;

    inline size_t payload_size() const { return payload_size_; };
    virtual size_t size() const = 0;
    inline Time base_ts() const { return base_ts_; };
    static Error error() { return error_; };
    
    /* Factory */
    static RecordSet *openFile(const std::string &varpath, Time base_ts, 
        StorageType storage, size_t payload_size);

protected:
    File file_;
    static Error error_;

    RecordSet(const File &file, size_t payload_size, Time base_ts)
    : file_(file), payload_size_(payload_size), base_ts_(base_ts) {};
    virtual off_t tsToIndex(Time ts) const = 0;

private:
    size_t payload_size_;
    Time base_ts_;
};


/**
 * A record set that is ordered and hence where random insertion is not allowed
 */
class OrderedRecordSet : public RecordSet {
public:
    class iterator;

    virtual ~OrderedRecordSet() {};

    /* Data access */
    Record &operator[](off_t i);
    inline Record &at(Time ts) { return operator[](tsToIndex(ts)); };
    inline Record &front() { return operator[](0); };
    inline Record &back() { return operator[](size()-1); };

    std::list<Record*> *between(Time from, Time to, size_t count);
    virtual Time insert(const void *value) = 0;
    virtual Time insert(Time ts, const void *value) = 0;

    /* Iterator */
    iterator begin();
    iterator end();

protected:
    OrderedRecordSet(const File &file, size_t payload_size, Time base_ts)
        : RecordSet(file, payload_size, base_ts) {};
    virtual Record *readRecordAt(off_t i) = 0;
};


// /**
//  * A generic record set, which allows random insertions and deletions
//  */
// class GenericRecordSet : public RecordSet {
// public:
//     GenericRecordSet(int file, size_t payloadSize);
//     ~GenericRecordSet();

//     Time insert(Time ts, const char *value);

//     static GenericRecordSet *createFile(const std::string &path);

//     /* Iterators */
//     // iterator begin();
//     // iterator end();
// };


/**
 * A slotted record set
 **/
class SlottedRecordSet : public OrderedRecordSet {
public:
    ~SlottedRecordSet() {};

    inline size_t size() const { return nrecords_; };
    
    Time insert(const void *value);
    Time insert(Time ts, const void *value);

    static SlottedRecordSet *fromFile(const File &file, size_t payload_size, Time base_ts);
    static SlottedRecordSet *create(const std::string &varpath, Time base_ts,
        size_t payload_size, Time interval);

private:
    Time interval_;
    size_t nrecords_;

    SlottedRecordSet(const File &file, size_t payload_size, Time base_ts);
    Record *readRecordAt(off_t i);
    inline off_t tsToIndex(Time ts) const;;
    inline Time indexToTs(off_t i) { return base_ts() + i*interval_; };
    inline off_t payloadOffset(off_t i)
        { return sizeof(interval_) + i*payload_size();};
};


/**
 * A stamped record set
 */
class StampedRecordSet : public OrderedRecordSet {
public:
    ~StampedRecordSet() {};

    inline size_t size() const { return times_.size(); };
    
    Time insert(const void *value);
    Time insert(Time ts, const void *value);

    static StampedRecordSet *fromFile(File &file, size_t payload_size, Time base_ts);
    static StampedRecordSet *create(const std::string &varpath, Time base_ts,
        size_t payload_size);

private:
    std::vector<Time> times_;

    StampedRecordSet(const File &file, size_t payload_size, Time base_ts);
    Record *readRecordAt(off_t i);
    off_t tsToIndex(Time ts) const;
    inline off_t tsOffset(off_t i) 
        { return i*(sizeof(Time) + payload_size());};
    inline off_t payloadOffset(off_t i)
        { return tsOffset(i) + sizeof(Time); };
};


class OrderedRecordSet::iterator
    : public std::iterator<std::random_access_iterator_tag, Record> {
public:
    iterator(OrderedRecordSet &set, off_t i) : set_(set), i_(i) {};
    iterator(const iterator &other) : set_(other.set_), i_(other.i_) {};

    /* Access operators */
    reference operator*() const { return set_[i_]; };
    pointer operator->() const { return &set_[i_]; };
    reference operator[](difference_type n) const { return set_[n]; };

    /* Traversal operators */
    iterator &operator++() { i_++; return *this;} ;
    iterator operator++(int) { iterator t = *this; i_++; return t;} ;
    iterator &operator--() { i_--; return *this;} ;
    iterator operator--(int) { iterator t = *this; i_--; return t;} ;

    iterator operator+(difference_type n) const { return iterator(set_, i_+n);};
    iterator &operator+=(difference_type n) { i_ += n; return *this; };
    iterator operator-(difference_type n) const { return iterator(set_, i_-n);};
    iterator &operator-=(difference_type n) { i_ -= n; return *this; };

    /* Comparison operators */
    bool operator==(const iterator& rhs) { return i_ == rhs.i_; };
    bool operator!=(const iterator& rhs) { return i_ != rhs.i_; };
    bool operator< (const iterator& rhs) { return i_ <  rhs.i_; };
    bool operator<=(const iterator& rhs) { return i_ <= rhs.i_; };
    bool operator> (const iterator& rhs) { return i_ >  rhs.i_; };
    bool operator>=(const iterator& rhs) { return i_ >= rhs.i_; };

protected:
    OrderedRecordSet &set_;
    off_t i_;
};

} /* namespace adb */

#endif /* ADB_RECORD_SET_HPP_ */
