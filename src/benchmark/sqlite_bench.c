#include <adb.h>
#include <stdio.h>
#include <stdlib.h>
#include <sqlite3.h>

static void
scalar_inserts(sqlite3 *db, int count, double val)
{
    int i, ret;
    sqlite3_stmt *stmt;
    
    // Prepare insertion statement
    const char *insert_sql = "insert into scalar (timestamp, value) values (?,?);";
    ret = sqlite3_prepare_v2(db, insert_sql, -1, &stmt, NULL);
    
    ret = sqlite3_exec(db, "BEGIN;", NULL, 0, NULL);
    for (i=0; i<count; i++) {
        ret = sqlite3_bind_int64(stmt, 1, adb_now());
        ret = sqlite3_bind_double(stmt, 2, val);
        ret = sqlite3_step(stmt);
        ret = sqlite3_reset(stmt);
    }
    ret = sqlite3_exec(db, "END;", NULL, 0, NULL);
    
    ret = sqlite3_finalize(stmt);
}

static void
vector_inserts(sqlite3 *db, int count, double *val, int n)
{
    int i, ret;
    sqlite3_stmt *stmt;
    
    // Prepare insertion statement
    const char *insert_sql = "insert into vector (timestamp, value) values (?,?);";
    ret = sqlite3_prepare_v2(db, insert_sql, -1, &stmt, NULL);
    
    ret = sqlite3_exec(db, "BEGIN;", NULL, 0, NULL);
    for (i=0; i<count; i++) {
        ret = sqlite3_bind_int64(stmt, 1, adb_now());
        ret = sqlite3_bind_blob(stmt, 2, val, sizeof(*val) * n, SQLITE_STATIC);
        ret = sqlite3_step(stmt);
        ret = sqlite3_reset(stmt);
    }
    ret = sqlite3_exec(db, "END;", NULL, 0, NULL);
    
    ret = sqlite3_finalize(stmt);
}

static void
queries(sqlite3 *db, int count)
{
    int ret;
    const char *query_sql = "select count(*) from scalar;";
    ret = sqlite3_exec(db, query_sql, NULL, 0, NULL);
}

int main(int argc, char **argv)
{
    int i;
    double val;
    adb_time start, end;
    int sctst[] = {100, 250, 500,
                   1000, 2500, 5000,
                   10000, 25000, 50000,
                   100000, 250000, 500000,
                   1000000};
    int sctst_count = 13;

    // Create database
    sqlite3 *db;
    char *zErrMsg = 0;
    int rc;
    const char *create_sql = "create table if not exists scalar"
                             "(timestamp integer primary key asc, value double);"
                             "create table if not exists vector"
                             "(timestamp integer primary key asc, value blob);";
    
    rc = sqlite3_open("sqlite.db", &db);
    sqlite3_exec(db, create_sql, NULL, 0, &zErrMsg);
    
    // Create scalar
    val = random();

    for (i=0; i < sctst_count; i++) {
        int test = sctst[i];
        
        // Time it
        start = adb_now();
        scalar_inserts(db, test, val);
        end = adb_now();
        printf("Test %i, %i scalar inserts: %lli us\n", i, test, end-start);
        
        start = adb_now();
        queries(db, test);
        end = adb_now();
        printf("Test %i, %i scalar query: %lli us\n", i, test, end-start);
    }
    
    // int vctst[] = {1, 3, 5, 10, 25, 50, 100};
    // int vctst_count = 7;    
    // int mat_dim = 640*480*3;
    // double mat[640*480*3];
    // for (i=0; i<640*480*3; i++) {
    //     mat[i] = random();
    // }
    
    // for (i=0; i < vctst_count; i++) {
    //     int test = vctst[i];
        
    //     // Time it
    //     start = adb_now();
    //     vector_inserts(db, test, mat, mat_dim);
    //     end = adb_now();
    //     printf("Test %i, %i vector inserts: %lli us\n", i, test, end-start);
    // }
    

    //scalar_inserts(db, test, val);
    // Create query
    ///const char *query_sql = "select * from var;";
    
    
    // Cleanup
    sqlite3_close(db);
    
    return 0;
}
