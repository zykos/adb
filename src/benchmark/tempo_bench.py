#!/usr/bin/python
from bench import Benchmark
from tempodb import Client, DataPoint


class TempoBenchmark(Benchmark):

    def __init__(self, options):
        self.client = Client(options['api_key'], options['secret'])
        self.variable = options['key']

    def prepare_data(self, keys, values):
        self.data = [DataPoint(k,v) for (k,v) in zip(keys,values)]

    def insert(self):
        self.client.write_key(self.variable, self.data)

    def query(self, start, end):
        self.client.read_key(self.variable, start, end)
