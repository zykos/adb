#!/bin/sh

db=bench.rrd

# Create DB
rrdtool create $db --start `date +%s` --step 1 DS:input:GAUGE:600:U:U RRA:LAST:0.5:1:24

tests="100 250 500 1000 2500 5000 10000 25000 50000"
#tests="1 2 3 4 5"


# Run tests
for count in $tests
do
	str="rrdtool update $db"
	for i in `seq 1 $count`
	do
		str+=" `date +%s`:$RANDOM"
	done
	t1=`date +%s.%N`
	`$str`
	t2=`date +%s.%N`
	diff=$(( $t2 - $t1 ))
	echo $count: $diff
done
