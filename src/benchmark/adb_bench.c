#include <adb.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>


#define RUNS 10


static adb_time
inserts(adb_var *var, int count, double *val)
{
    int i, ret;
    adb_time start, end;
    var->opmode = ADB_WRMODE;
    
    // Start timer
    start = adb_now();
    // Open variable
    ret = adb_open(var);
    if (ret <0) return 0;
    
    // Loop, inserting
    for (i=0; i < count; i++) {
        adb_insert(var, adb_now(), val);
    }
    
    // Close
    adb_close(var);
    // End timer
    end = adb_now();
    
    return end-start;
}

static adb_time
queries(adb_var *var, int count, double *val)
{
    int i, ret;
    adb_time start, end;
    var->start = ADB_BEGINNING;
    var->opmode = ADB_RDMODE;
    
    // Start timer
    start = adb_now();
    // Open variable
    ret = adb_open(var);
    if (ret <0) return 0;
    
    // Loop, summing
    for (i = 0; i < count; i++) {
        *val += *(double *)adb_value(var);
        adb_next(var);
        
    }
    
    // Close
    adb_close(var);
    // End timer
    end = adb_now();
    
    return end-start;
}


static int sctst[] =
{   100, 250, 500,
    1000, 2500, 5000,
    10000, 25000, 50000,
    100000, 250000, 500000,
    1000000
};

static int vctst[] = {1, 3, 5, 10, 25, 50, 100};

typedef struct {
    adb_time scalar_inserts[13];
    adb_time scalar_queries[13];
    adb_time vector_inserts[7];
    adb_time vector_queries[7];
} stats;


static int scalar_tests(stats * times)
{
    int i;
    adb_time lapse;
    adb_time sum_time;
    
    // Create scalar
    double val = 1;
    
    // Create variable
    mkdir("db", 0755);
    mkdir("db/scalar", 0755);
    adb_var var;
    var.path = "db/scalar";
    var.start = ADB_NEWBLOCK;
    var.type.storage = ADB_STAMPED;
    var.type.size = sizeof(val);
    
    for (i = 0; i < 13; i++) {
        int test = sctst[i];
        
        sum_time = 0;
        for (int j = 0; j < RUNS; j++) {
            // Time it
            lapse = inserts(&var, test, &val);
            if (lapse == 0) {
                printf("scalar inserts failed\n");
                return -1;
            }
            sum_time += lapse;
            printf(".");
            fflush(stdout);
            var.start = ADB_OLDBLOCK;
        }
        times->scalar_inserts[i] = sum_time / RUNS;
        

        sum_time = 0;
        for (int j = 0; j < RUNS; j++) {
            lapse = queries(&var, test, &val);
            if (lapse == 0) {
                printf("scalar queries failed\n");
                return -1;
            }
            sum_time += lapse;
            printf("?");
            fflush(stdout);
        }
        times->scalar_queries[i] = sum_time / RUNS;
    }
    return 0;
}

static int vector_tests(stats * times)
{
    int i;
    adb_time lapse;
    adb_time sum_time;
    
    // Create vector
    int mat_dim = 640*480*3;
    double mat[640*480*3];
    for (i=0; i<640*480*3; i++) {
        mat[i] = random();
    }
    
    // Create variable
    mkdir("db", 0755);
    mkdir("db/vector", 0755);
    adb_var var;
    var.path = "db/vector";
    var.start = ADB_NEWBLOCK;
    var.type.storage = ADB_STAMPED;
    var.type.size = mat_dim * sizeof(*mat);
    
    
    for (i = 0; i < 7; i++) {
        int test = vctst[i];
        
        // Time it
        sum_time = 0;
        for (int j = 0; j < RUNS; j++) {
            lapse = inserts(&var, test, mat);
            if (lapse == 0) {
                printf("vector inserts failed\n");
                return -1;
            }
            printf(":");
            fflush(stdout);
            sum_time += lapse;
            var.start = ADB_OLDBLOCK;
        }
        times->vector_inserts[i] = sum_time / RUNS;
        
        double sum = 0;
        sum_time = 0;
        for (int j = 0; j < RUNS; j++) {
            lapse = queries(&var, test, &sum);
            if (lapse == 0) {
                printf("vector queries failed\n");
                return -1;
            }
            sum_time += lapse;
            printf("!");
            fflush(stdout);
        }
        times->vector_queries[i] = sum_time / RUNS;
        
        var.start = ADB_OLDBLOCK;
    }
    
    return 0;
}

int main(int argc, char **argv)
{
    stats times;
    
    // Run the actual tests
    scalar_tests(&times);
    vector_tests(&times);
    
    // Print the averages
    printf("Scalar tests:\n");
    for (int i = 0; i < 13; i++) {
        printf("%i\t%lli\t%lli\n",
        sctst[i],
        times.scalar_inserts[i],
        times.scalar_queries[i]);
    }
    printf("Vector tests:\n");
    for (int i = 0; i < 7; i++) {
        printf("%i\t%lli\t%lli\n",
        vctst[i],
        times.vector_inserts[i],
        times.vector_queries[i]);
    }
    
}
