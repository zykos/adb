from bench import Benchmark
import calendar
import sqlite3

class SqliteBenchmark(Benchmark):

    def __init__(self, options):
        self.conn = sqlite3.connect(options['path'])
        self.cur = self.conn.cursor()
        self.type = options['type']
        self.cur.execute('create table if not exists pairs \
                          (ts integer primary key asc, val %s);' % self.type)

    def prepare_data(self, keys, values):
        self.data = zip([dt2ts(k) for k in keys],values)

    def insert(self):
        self.cur.executemany("insert into pairs (ts, val) values (?,?);", 
            self.data)

    def prepare_query(self, start, end):
        self.start = start
        self.end = end

    def query(self, start, end):
        self.cur.execute("select * from pairs where ts > %s and ts > %s" %
            dt2ts(start), dt2ts(end))


def dt2ts(dt):
    return calendar.timegm(dt.timetuple())