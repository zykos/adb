from sqlite_bench import SqliteBenchmark
from rrdtool_bench import RrdBenchmark
from tempo_bench import TempoBenchmark
from datetime import datetime
from random import random
from timeit import Timer
import time


sqlite_blob = SqliteBenchmark({'path': 'db.sqlite3', 'type': 'blob'})
sqlite_double = SqliteBenchmark({'path': 'db.sqlite3', 'type': 'double'})
tempo = TempoBenchmark({'api_key': '405a63e7f1254eb8992760d2d4540349',
                        'secret': '43298ed3531b4aca9bc5bb42197e5022',
                        'key': 'test'})
'''
rrd = RrdBenchmark('path': 'db.rrd', 'type': 'DS:foo:GAUGE:20:0:U')

'''
scalar_marks = [1000000]
'''
scalar_marks = [1,2,3]
'''

if __name__ == '__main__':
    times = {'sqlite':{}, 'tempo':{}}

    # Scalar inserts
    

    for mark in scalar_marks:
        keys = [datetime.now() for i in range(0,mark)]
        values = [random() for k in keys]

        #Prepare values
        #sqlite_double.prepare_data(keys, values)
        #tempo.prepare_data(keys, values)
        #rrd.prepare_data(keys, values)

        #sqlite_double_t = Timer("sqlite_double.insert", "from __main__ import sqlite_double")
        #tempo_t = Timer("tempo.insert", "from __main__ import tempo")
        #rrd_t = Timer("rrd.insert", "from __main__ import rrd")

        #times['sqlite'][mark] = sqlite_double_t.timeit(1)
        #times['tempo'][mark] = tempo_t.timeit(1)
        #times['rrd'][mark] = rrd_t.timeit()

        # TempoDB
        tempo.prepare_data(keys, values)
        t1 = time.time()
        tempo.insert()
        t2 = time.time()
        times['tempo'][mark] = t2-t1

    print times