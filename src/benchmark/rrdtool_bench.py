#!/usr/bin/env python
from bench import Benchmark
# import sys
# sys.path.append('/path/to/rrdtool/lib/python2.6/site-packages/')
# import rrdtool, tempfile

# DAY = 86400
# YEAR = 365 * DAY
# fd,path = tempfile.mkstemp('.png')

# rrdtool.graph(path,
#               '--imgformat', 'PNG',
#               '--width', '540',
#               '--height', '100',
#               '--start', "-%i" % YEAR,
#               '--end', "-1",
#               '--vertical-label', 'Downloads/Day',
#               '--title', 'Annual downloads',
#               '--lower-limit', '0',
#               'DEF:downloads=downloads.rrd:downloads:AVERAGE',
#               'AREA:downloads#990033:Downloads')

# info = rrdtool.info('downloads.rrd')
# print info['last_update']
# print info['ds[downloads].minimal_heartbeat']


class RrdBenchmark(Benchmark):
    def __init__(self, options):
        rrdtool.create(options['path'], options['type'])

    def prepare_data(self, key, values):
        pass

    def insert(self):
        pass

    def query(self, start, end):
        pass
