/*******************************************************************************
 adb_mattype.h
 adb
 
 Copyright (c) 2012, Michael Sokolnicki
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 
 
 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include "mex.h"
#include <adb.h>

struct mattype {
    mwSize ndim;
    mwSize *dims;
    mxClassID classid;
    mxComplexity ComplexFlag;
};

void
mattype_pack(const struct mattype *type, char *arr)
{
    char typechar;
    switch (type->classid) {
        case mxUNKNOWN_CLASS:  typechar =  0; break;
        case mxCELL_CLASS:     typechar =  1; break;
        case mxSTRUCT_CLASS:   typechar =  2; break;
        case mxLOGICAL_CLASS:  typechar =  3; break;
        case mxCHAR_CLASS:     typechar =  4; break;
        case mxVOID_CLASS:     typechar =  5; break;
        case mxDOUBLE_CLASS:   typechar =  6; break;
        case mxSINGLE_CLASS:   typechar =  7; break;
        case mxINT8_CLASS:     typechar =  8; break;
        case mxUINT8_CLASS:    typechar =  9; break;
        case mxINT16_CLASS:    typechar = 10; break;
        case mxUINT16_CLASS:   typechar = 11; break;
        case mxINT32_CLASS:    typechar = 12; break;
        case mxUINT32_CLASS:   typechar = 13; break;
        case mxINT64_CLASS:    typechar = 14; break;
        case mxUINT64_CLASS:   typechar = 15; break;
        case mxFUNCTION_CLASS: typechar = 16; break;
    } 
}

void
mattype_unpack(struct mattype *type, const char *arr)
{
    char typechar;
    
    switch (typechar & ~0x80) {
        case  0: type->classid = mxUNKNOWN_CLASS;  break;
        case  1: type->classid = mxCELL_CLASS;     break;
        case  2: type->classid = mxSTRUCT_CLASS;   break;
        case  3: type->classid = mxLOGICAL_CLASS;  break;
        case  4: type->classid = mxCHAR_CLASS;     break;
        case  5: type->classid = mxVOID_CLASS;     break;
        case  6: type->classid = mxDOUBLE_CLASS;   break;
        case  7: type->classid = mxSINGLE_CLASS;   break;
        case  8: type->classid = mxINT8_CLASS;     break;
        case  9: type->classid = mxUINT8_CLASS;    break;
        case 10: type->classid = mxINT16_CLASS;    break;
        case 11: type->classid = mxUINT16_CLASS;   break;
        case 12: type->classid = mxINT32_CLASS;    break;
        case 13: type->classid = mxUINT32_CLASS;   break;
        case 14: type->classid = mxINT64_CLASS;    break;
        case 15: type->classid = mxUINT64_CLASS;   break;
        case 16: type->classid = mxFUNCTION_CLASS; break;
    }
}
