% Copyright (c) 2012, Michael Sokolnicki
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met: 
%
% 1. Redistributions of source code must retain the above copyright notice, this
% list of conditions and the following disclaimer. 
% 2. Redistributions in binary form must reproduce the above copyright notice,
% this list of conditions and the following disclaimer in the documentation
% and/or other materials provided with the distribution. 
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
% ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

classdef adbVar
    %ADBVAR Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        path
        type
    end
    
    properties (SetAccess = private)
        handle
    end
    
    methods
        %% Constructor
        function obj = adbVar(path, type, mode, start)
            obj.path = path;
            obj.type = type;
            obj.handle = adbm_open(path, type, mode, start);
        end
        
        %% Indexing override
        function sref = subsref(obj, s)
           switch s(1).type
              % Use the built-in subsref for dot notation
              case '.'
                 sref = builtin('subsref',obj,s);
              case '()'
                %% TODO
              % No support for indexing using '{}'
              case '{}'
                 error('adb:subsref',...
                   'Not a supported subscripted reference')
           end 
        end
        
        function sref = subsasgn(obj,s,val)
            switch s(1).type
              % Use the built-in subsref for dot notation
              case '.'
                 sref = builtin('subsasgn',obj,s,val);
              case '()'
                %% TODO
              % No support for indexing using '{}'
              case '{}'
                 error('adb:subsasgn',...
                   'Not a supported subscripted reference')
           end
        end
    end
end
