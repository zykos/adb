/*******************************************************************************
 adb_query.c
 adb
 
 Copyright (c) 2012, Michael Sokolnicki
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 
 
 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include "mex.h"
#include <adb.h>

void
mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
adb_var var = {0};
    adb_time key, *keys = NULL;
    char *values;
    mwSize rows;
    char *path;
    char errmsg[100];
    int i, pathlen, status;
    
    /* open adb variable for writing */
    var.opmode = ADB_WRMODE;

    /* check for proper number of arguments */
    if (nrhs < 3) {
        mexErrMsgIdAndTxt(COMPONENT "nrhs","Not enough input arguments.");
    } else if (nrhs > 3) {
        mexErrMsgIdAndTxt(COMPONENT "nrhs","Too many input arguments.");
    }
    if (nlhs > 1) {
        mexErrMsgIdAndTxt(COMPONENT "nlhs","Only one output available.");
    }
    /* make sure the first input argument is string */
    if (!mxIsChar(prhs[0])) {
        mexErrMsgIdAndTxt(COMPONENT "type","PATH must be a string.");
    }
    if (!mxIsInt64(prhs[1])) {
        mexErrMsgIdAndTxt(COMPONENT "type","T must be an array of int64.");
    }
    
    
    /* extract the path */
    pathlen = mxGetN(prhs[0]) * sizeof(mxChar) + 1;
    var.path = path = mxMalloc(pathlen);
    status = mxGetString(prhs[0], path, pathlen);
    
    /* get the times matrix */
    if (mxGetM(prhs[1]) == 1) {
        key = *(adb_time *)mxGetData(prhs[1]);
    } else if (mxGetM(prhs[1]) == rows) {
        keys = mxGetData(prhs[1]);
    } else {
        mexErrMsgIdAndTxt(COMPONENT "dimsMismatch",
        "T must either be a scalar or have the same number or rows as X.");
    }
    
    if (nrhs == 4) {
        /* create a new block */
        var.start = ADB_NEWBLOCK;
        
        var.storage = ADB_STAMPED; // change
        var.size = mxGetN(prhs[2]) * elementSize(prhs[2]);
    } else {
        /* write to an existing block */
        var.start = ADB_OLDBLOCK;
    }

    /* open handle */
    status = adb_open(&var);
    if (status < 0) {
        sprintf(errmsg, "Open was unsuccessful with errno=%d.", errno);
        mexErrMsgIdAndTxt(COMPONENT "adb_open", errmsg);
                
    }
    
    for (i = 0; i < rows; i++) {  
        if (keys) key = keys[i];
        status = adb_insert(&var, key, values + i*var.size);
        if (status < 0) {
            sprintf(errmsg, "Insert was unsuccessful with errno=%d.", errno);
            mexErrMsgIdAndTxt(COMPONENT "adb_insert", errmsg);

        }
    }
    
    /* close handle */
    status = adb_close(&var);
    if (status < 0) {
        sprintf(errmsg, "Close was unsuccessful with errno=%d.", errno);
        mexErrMsgIdAndTxt(COMPONENT "adb_close", errmsg);
                
    }
    mxFree(path);
}
