from ctypes import *
from datetime import datetime
from numpy import *
import os
import dateutil.parser

# Windows
# Linux
# Mac OS
libadb = CDLL("libadb.dylib")

adb_time    = c_longlong
adb_store   = c_longlong
adb_mode    = c_int

def ret(e):
    if e < 0:
        raise Exception


class adb_type(Structure):
    _fields_ = [("storage", adb_store),
                ("size", c_uint),
                ("info", c_char * 25)]

class adb_var(Structure):
    _fields_ = [("path", c_char_p),
                ("opmode", adb_mode),
                ("start", adb_time),
                ("type", adb_type),
                ("clnnfo", c_void_p),
                ("rblock", c_void_p),
                ("wblock", c_void_p)]

adb_now     = libadb.adb_now
adb_open    = libadb.adb_open
adb_close   = libadb.adb_close
adb_key     = libadb.adb_key
adb_value   = libadb.adb_value
adb_insert  = libadb.adb_insert
adb_remove  = libadb.adb_remove
adb_prev    = libadb.adb_prev
adb_next    = libadb.adb_next

# Argument types
adb_now.argtypes    = []
adb_open.argtypes   = [POINTER(adb_var)]
adb_close.argtypes  = [POINTER(adb_var)]
adb_key.argtypes    = [POINTER(adb_var)]
adb_value.argtypes  = [POINTER(adb_var)]
adb_insert.argtypes = [POINTER(adb_var), adb_time, c_void_p]
adb_remove.argtypes = [POINTER(adb_var)]
adb_prev.argtypes   = [POINTER(adb_var)]
adb_next.argtypes   = [POINTER(adb_var)]

# Return types
adb_now.restype     = adb_time
adb_open.restype    = ret
adb_close.restype   = ret
adb_key.restype     = adb_time
adb_value.restype   = c_void_p
adb_insert.restype  = ret
adb_remove.restype  = ret
adb_prev.restype    = ret
adb_next.restype    = ret


WRITE_MODE  = 0
READ_READ   = 1
CLEAN_MODE  = 2

STAMPED = 0
CSTRING = -1
def SLOTTED(g):
    return g

class Time(object):
    def __init__(self, time):
        if type(time) == int:
            self._time = time
        elif type(time) == datetime:
            self._time = Time.from_dt(dt)._time
        elif type(time) == str:
            dt = dateutil.parser.parse(time)
            self._time = Time.from_dt(dt)._time
        else:
            raise Exception
    
    def __str__(self):
        return self.to_dt().isoformat()
    
    def __repr__(self):
        return "Time('%s')" % self.__str__()
    
    def _get_value(self):
        return self._time
    
    value = property(_get_value)
    
    @staticmethod
    def from_dt(dt):
        us = int((dt - Time(0).to_dt()).total_seconds() * 1000000)
        return Time(us)
    
    def to_dt(self):
        ts, us = self._time / 1000000, self._time % 1000000
        dt = datetime.utcfromtimestamp(ts)
        dt = dt.replace(microsecond  = us)
        return dt

def open(path, mode, start = 0):
    var = Var(adb_var(c_char_p(path), mode, start,
              adb_type(), None, None, None))
    adb_open(var.p)

def create(path, storage, size, info = ''):
    if not os.path.exists(path):
        os.makedirs(path)
    type = adb_type(storage, size, info)
    var = Var(adb_var(c_char_p(path), WRITE_MODE, 1, type, None, None, None))
    adb_open(var.p)
    return var


class Var(object):
    def __init__(self, var):
        self._var = var
    
    '''
    , path, mode = WRITE_MODE, start = 0):
        if os.path.exists(path):
            # Open existing var
            self._var = Var(adb_var(c_char_p(path), mode, start,
                              adb_type(), None, None, None))
            adb_open(self.p)
        else:
            # Create new var
            os.makedirs(path)
            self._var = Var(adb_var(c_char_p(path), WRITE_MODE, 1,
                                    adb_type(), None, None, None))
                        
    def _delayed_create(self, tmpl):
        size = ...
        info = ...
        self._var.type = adb_type(STAMPED, size, info)
        var = Var(adb_var(c_char_p(path), WRITE_MODE, 1, type, None, None, None))
                adb_open(var.p)
        adb_open(self.p)
    '''
    
    def __repr__(self):
        return "Var('%s')" % self.path
    
    def _get_p(self):
        return byref(self._var)

    def _get_path(self):
        return str(self._var.path)

    def _get_opmode(self):
        if self._var.opmode == WRITE_MODE:
            return "W"
        elif self._var.opmode == READ_MODE:
            return "R"
        elif self._var.opmode == CLEAN_MODE:
            return "C"
        else:
            raise Exception

    def _get_size(self):
        return int(self._var.type.size)
    
    def _get_info(self):
        return str(self._var.type.info)
    
    p = property(_get_p)
    path = property(_get_path)
    opmode = property(_get_opmode)
    size = property(_get_size)
    info = property(_get_info)
    
    def __getitem__(self, key):
        pass
    
    def __setitem__(self, key, val):
        pass

    def __del__(self):
        adb_close(self.p)

    def key(self):
        return Time(adb_key(self.p))

    def value(self):
        return adb_value(self.p)

    def insert(self, time, val):
        return adb_insert(self.p, time.value, array(val).ctypes.data)

    def remove(self):
        return adb_remove(self.p)

    
def now():
    return Time(adb_now())


