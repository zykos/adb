#include <adb.h>
#include <Python.h>
#include <numpy/arrayobject.h>
#include <stdlib.h>

typedef struct {
	int typenum;
	npy_intp dims[5];
} data_layout;

int arr2type(PyObject *arr, adb_type *type)
{
	data_layout *layout;
	
	type->size = PyArray_ITEMSIZE(arr) * PyArray_SIZE(arr);
	type->storage = ADB_STAMPED;
	// Fill type->info
	layout = (data_layout *)&type->info;
	layout->typenum = PyArray_TYPE(arr);
	memcpy(layout->dims, PyArray_DIMS(arr), 5 * sizeof(npy_intp));
	return 1;
}

PyObject *create_arr(adb_type *type, char *data)
{
	PyObject *arr;
	data_layout *layout;
	int nd;

	layout = (data_layout *)&type->info;
	for (nd = 0; layout->dims[nd]; nd++);

	arr = PyArray_SimpleNew(nd, layout->dims, layout->typenum);
	memcpy(PyArray_DATA(arr), data, type->size);
	return arr;
}

int main() {

}
