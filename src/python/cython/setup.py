## usage: python setpu.py build_ext --inplace
from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
import numpy

setup(
  name = 'adb Python wrapper',
  cmdclass = {'build_ext': build_ext},
  ext_modules = [Extension("adb", ["cadb.pyx"])]
)
