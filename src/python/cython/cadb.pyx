from libc.stdint cimport int64_t
from libc.stddef cimport size_t
from numpy import array, dtype
from numpy cimport *

cdef extern from "stdlib.h":
	void *memcpy(void *dst, void *src, long n)

import_array()

cdef extern from "adb.h":
	ctypedef int64_t	adb_time
	ctypedef int64_t	adb_store

	ctypedef enum adb_mode:
		ADB_WRMODE = 0
		ADB_RDMODE = 1
		ADB_CLMODE = 2

	ctypedef struct adb_type:
		adb_store	storage
		size_t		size
		char		info[25]

	ctypedef struct adb_var:
		char*		path
		adb_mode	opmode
		adb_time	start
		adb_type	type

	adb_time adb_now()
	int adb_open(adb_var* v)
	int adb_close(adb_var* v)
	adb_time adb_key(adb_var* v)
	void* adb_value(adb_var* v)
	int adb_insert(adb_var* v, adb_time key, void* value)
	int adb_remove(adb_var* v)
	int adb_prev(adb_var* v)
	int adb_next(adb_var* v)

###

ctypedef struct data_layout:
	int typenum
	npy_intp dims[5]

cdef int arr2type(arr, adb_type *type):
	cdef data_layout* layout
	
	type.size = PyArray_ITEMSIZE(arr) * PyArray_SIZE(arr)
	type.storage = 0
	layout = <data_layout *>type.info
	layout.typenum = PyArray_TYPE(arr)
	memcpy(layout.dims, PyArray_DIMS(arr), 5 * sizeof(npy_intp))
	return 1

cdef create_arr(adb_type *type, void *data):
	cdef data_layout *layout
	cdef int nd

	layout = <data_layout *>type.info
	nd = 0
	while layout.dims[nd]:
		nd += 1

	arr = PyArray_SimpleNew(nd, layout.dims, layout.typenum)
	memcpy(PyArray_DATA(arr), data, type.size)
	return arr


###

# Write type information for an array
#cdef int write_type(ndarray arr, adb_type *type):
#	cdef char* encoded_str
#	cdef int n
#
#	dimensions = str([i for i in arr.shape[:arr.ndim]])
#	tmp_str = arr.dtype.str + ':' + dimensions + ':\0'
#	encoded_str = tmp_str
#	n = len(tmp_str)
#
#	type.storage = 0
#	type.size = arr.nbytes
#	memcpy(type.info, encoded_str, n+1)

def get_array_info(arr):
	cdef adb_type type
	arr2type(arr, &type)
	return str(type.info)

a = array([[1,2,3],[4,5,6]])

# Create empty array from the type information
#cdef ndarray create_array(adb_type *type, void *data):
#	type_str = type.info
#	dtype_str, shape_str, _ = type_str.split(':')
#	dt = dtype(dtype_str)
#	shape = list(shape_str)
#	n = shape.ndim
#
#	arr = PyArray_SimpleNew(n, shape, dt)
#	memcpy(PyArray_DATA(arr), data, arr.nbytes)
#	return arr

def set_info_array(info, ndarray arr):
	cdef adb_type type
	cdef char* info_str = info
	cdef ndarray new_arr
	type.storage = 0
	type.size = 100
	memcpy(type.info, info_str, len(info))
	new_arr = create_arr(&type, arr.data)
	return array(new_arr).shape

def identarray(arr):
	cdef adb_type type
	arr2type(arr, &type)
	ar2 = create_arr(&type, PyArray_DATA(arr))
	return ar2

#cdef int insert_many(char *path, keys, values):
#	cdef int ret
#	cdef adb_var var
#
#	# Initialize var
#	var.path = path
#	var.opmode = ADB_WRMODE
#	var.start = 1
#
#	# Open, write, close
#	ret = adb_open(&var)
#	if ret != 0:
#		return ret
#	for key, val in keys, values:
#		ret = adb_insert(&var, key, <void *>val.data)
#		if ret != 0:
#			return ret
#	adb_close(&var)
#	return 0

class Var(object):

	def __init__(self, opts):
		pass

	def __getitem__(self, key):
		pass

	def __setitem__(self, key, value):
		pass

	def _time_from_key(self, key):
		pass

