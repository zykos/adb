cimport cadb
from datetime import datetime, timedelta
import numpy as np

cdef class Variable:
    cdef cadb.var _var

    def __cinit__(self, path):
        self._var.path = path

    def __getitem__(self, key):
        cdef int ret
        cdef cadb.time stop
        results = {}

        if type(key) is slice:
            self._var.start = dt2ts(key.start)
            stop = dt2ts(key.stop)
            # TODO: step
        elif type(key) is datetime:
            self._var.start = dt2ts(key)
        else:
            raise TypeError()
        
        # Open handle
        self._var.opmode = ADB_RDMODE
        ret = cadb.open(&self._var)
        if ret < 0:
            raise Error()
        # Read
        while True:
            # TODO: Read operation
            ret = cadb.next(&self._var)
            # Boundary condition
            if ret == 1 or cadb.key(&self._var) >= stop:
                break
        # Close handle
        ret = cadb.close(&self._var)
        if ret < 0:
            raise Error()
        return results

    def __setitem__(self, key, value):
        if type(key) is slice:
            print "slice"
        elif type(key) is datetime:
            print "single"
        else:
            print "error"

    def __dealloc__(self):
        pass

