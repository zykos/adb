#include "adb.h"
#include <stdlib.h>
#include "Numeric/arrayobject.h"


static int write_type(PyObject *arr, adb_type *type)
{
    int *info = type->info;
    int type_num = PyArray_TYPE(arr);
    int ndims;
    int *dims = PyArray_DIMS(arr);

    PyArray_GetArrayParamsFromObject(arr, NULL, 0, NULL, &ndims, &dims, NULL, NULL);

    info[0] = type_num;
    info[1] = ndims
    memcpy(&info[2], dims, ndims * sizeof(int));
    
    
    type->storage = ADB_STAMPED;
    type->size = PyArray_NBYTES(arr);
}

static PyObject *array_from_adb(adb_type *type, void *data)
{
    int *info = type->info;
    int type_num = info[0];
    int ndim = info[1];
    int *dims = &info[2];

    PyObject *arr = PyArray_SimpleNew(ndims, dims, type_num);
    memcpy(PyArray_DATA(arr), data, type->size);
    return arr;
}
    
