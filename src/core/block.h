/*******************************************************************************
 block.h
 adb
 
 Copyright (c) 2012, Michael Sokolnicki
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 
 
 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef ADB_BLOCK_SET_H
#define ADB_BLOCK_SET_H


#include "adb.h"
#include <unistd.h>
#include <stdio.h>

typedef struct rb {
    adb_type    h;
    
    adb_time    base;
    off_t       cur;
    size_t      elms;
    size_t      datalen;
    char        *datamap;
    adb_time    *keys;
    int         datafd;
    int         keyfd;
} rblock;

rblock *
block_ropen(const char *path, adb_time base);

void
block_rclose(rblock *blk);

adb_time
block_curkey(rblock *blk);

char *
block_curdata(rblock *blk);

#define block_atend(blk)    (blk->cur == blk->elms-1)
#define block_atbeg(blk)    (blk->cur == 0)
#define block_movefwd(blk)  (blk->cur++)
#define block_movebwd(blk)  (blk->cur--)

int
block_moveto(rblock *blk, adb_time key);

/******************************************************************************/

typedef struct wb {
    adb_type    h;
    
    adb_time    base;
    adb_time    last;
    FILE        *datafs;
    FILE        *keyfs;
} wblock;

wblock *
block_create(const char *path, adb_time base, adb_type *h);

wblock *
block_wopen(const char *path, adb_time base);

void
block_wclose(wblock *blk);

int
block_write(wblock *blk, adb_time key, const char *value);

int
block_remove(wblock *blk);


#endif
