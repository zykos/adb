/*******************************************************************************
 var.h
 adb
 
 Copyright (c) 2012, Michael Sokolnicki
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 
 
 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#include "var.h"

#include <errno.h>      /* errno and errors */
#include <fcntl.h>      /* file creation flags, lock and flags */
#include <dirent.h>     /* dir, dirent, and the dir manipulation functions */
#include <stdlib.h>     /* system and qsort */
#include <stdio.h>      /* snprintf and rename */
#include <unistd.h>     /* read, write and close */


#define PARAMS  ".var"
#define CLEAN   ".clean"


// Helper functions

// adb_time comparison function
static int
cmp_time (const void *arg1, const void *arg2) {
    return (adb_time)arg1 < (adb_time)arg2 ? -1 :
          ((adb_time)arg1 > (adb_time)arg2 ?  1 : 0);
}

static int
store(const char *path, const char *fn, void *data, size_t n)
{
    char filepath[PATH_MAX];
    snprintf(filepath, PATH_MAX, "%s/%s" , path, fn);
    
    int fd = open(filepath, O_CREAT | O_RDWR);
    if (fd < 0) return -1;
    int ret = flock(fd, LOCK_EX);
    if (ret < 0) return -1;
    ret = (int)write(fd, data, n);
    close(fd);
    if (ret != n) return -1;
    return 0;
}

static int
recall(const char *path, const char *fn, void *data, size_t n)
{
    char filepath[PATH_MAX];
    snprintf(filepath, PATH_MAX, "%s/%s" , path, fn);
    
    int fd = open(filepath, O_RDONLY);
    if (fd < 0) return -1;
    int ret = flock(fd, LOCK_SH);
    if (ret < 0) return -1;
    ret = (int)read(fd, data, n);
    close(fd);
    if (ret != n) return -1;
    return 0;
}

static void
wait_on_clean(const char *path)
{
    char cln_path[PATH_MAX];
    
    // Wait for the lock on the clean path
    CLEANPATH(path, cln_path);
    int fd = open(cln_path, O_RDONLY);
    if (fd) {
        flock(fd, LOCK_SH);
        close(fd);
    }
}

static int
move_blocks(const char *src, const char *dst, adb_time *blocks, size_t count)
{
    int i, ret = 0;
    char dat_src[PATH_MAX];
    char dat_dst[PATH_MAX];
    char aux_src[PATH_MAX];
    char aux_dst[PATH_MAX];
    
    // Move them one by one
    for (i = 0; i < count; i++) {
        ret = DATFILE(src, blocks[i], dat_src);
        if (ret < 0) goto error;
        ret = AUXFILE(src, blocks[i], aux_src);
        if (ret < 0) goto error;
        
        ret = DATFILE(dst, blocks[i], dat_dst);
        if (ret < 0) goto error;
        ret = AUXFILE(dst, blocks[i], aux_dst);
        if (ret < 0) goto error;
        
        ret = rename(dat_src, dat_dst);
        if (ret < 0) goto error;
        
        ret = rename(aux_src, aux_dst);
        if (ret < 0) goto error;
    }
    
    return (int)count;
error:
    // Rollback
    for (; i > 0; i--) {
        ret = DATFILE(src, blocks[i], dat_src);
        if (ret < 0) goto error;
        ret = AUXFILE(src, blocks[i], aux_src);
        if (ret < 0) goto error;
        
        ret = DATFILE(dst, blocks[i], dat_dst);
        if (ret < 0) goto error;
        ret = AUXFILE(dst, blocks[i], aux_dst);
        if (ret < 0) goto error;
        
        ret = rename(dat_dst, dat_src);
        if (ret < 0) goto error;
        
        ret = rename(aux_dst, aux_src);
        if (ret < 0) goto error;
    }
    
    return ret;
}

static int
remove_blocks(const char *path, adb_time *blocks, size_t n)
{
    int ret, i;
    char dat_path[PATH_MAX];
    char aux_path[PATH_MAX];
    
    // Remove all files
    for (i = 0; i < n; i++) {
        ret = DATFILE(path, blocks[i], dat_path);
        if (ret < 0) goto error;
        ret = AUXFILE(path, blocks[i], aux_path);
        if (ret < 0) goto error;
        
        ret = remove(dat_path);
        if (ret < 0) goto error;
        
        ret = remove(aux_path);
        if (ret < 0) goto error;
    }
error:
    return ret;
}

static int
set_clean_manifest(const char *path, adb_time from, adb_time to)
{
    adb_time tss[2];
    tss[0] = from;
    tss[1] = to;
        
    return recall(path, CLEAN, tss, 2);
}



// Main functions

int
var_cleanup(const char *path, adb_time write_block, adb_time *read_blocks, size_t n)
{   
    int ret = 0, fd = 0;
    char cln_path[PATH_MAX];
    char tmp_path[PATH_MAX];
    
    // Generate paths
    ret = CLEANPATH(path, cln_path);
    if (ret < 0) goto error;
    ret = TEMP_PATH(path, tmp_path);
    if (ret < 0) goto error;
    
    // Lock the clean path (rendez-vous for waiting handles)
    fd = open(cln_path, O_RDONLY);
    ret = flock(fd, LOCK_EX);
    if (ret < 0) goto error;
    
    // Write the clean manifest
    ret = set_clean_manifest(path, read_blocks[0], read_blocks[n-1]);
    if (ret < 0) goto error;

    // Move read blocks to temp directory
    ret = move_blocks(path, tmp_path, read_blocks, n);
    if (ret < 0) goto error;
        
    // Move new block to root
    ret = move_blocks(cln_path, path, &write_block, 1);
    
    // Remove old blocks
    ret = remove_blocks(tmp_path, read_blocks, n);
    remove(tmp_path);
    
    // Or:
    char *cmd;
    ret = asprintf(&cmd, "rm -rf %s", tmp_path);
    if (ret < 0) goto error;
    ret = system(cmd);
    
    // TODO: finish
    
error:
    // Unlock the clean path
    if (fd) close(fd);
    
    return ret;
}

int
var_recover(const char *path)
{
    int ret, cln_fd;
    char cln_path[PATH_MAX];
    adb_time bounds[3];
    
    CLEANPATH(path, cln_path);
    
    // Recovery turning point is the existance of a clean manifest with no lock
    // on the clean directory.
    
    // Check for the clean manifest
    ret = recall(path, CLEAN, bounds, sizeof(bounds));
    if (ret < 0) {
        if (errno == ENOENT) {
            // There is no clean manifest
            // any cleaning operations have completed successfully
            ret = 0;
        }
        return ret;
    }
    
    // Manifest exists
    cln_fd = open(cln_path, O_RDONLY);
    ret = flock(cln_fd, LOCK_SH | LOCK_NB);
    if (ret < 0) {
        // Could not lock
        if (errno == EWOULDBLOCK) {
            // Lock exists, the variable is being cleaned, exit
            ret = 0;
        }
        close(cln_fd);
        return ret;
    }
    
    // The lock was successful, we have a recover situation
    
    // Unlock and close
    ret = flock(cln_fd, LOCK_UN);
    if (ret < 0) return ret;
    close(cln_fd);
    
    // Recovery
    // TODO: implement
    
    return 0;
}

adb_time
var_blockat(const char *path, adb_time ts)
{
    adb_time bts, hlb = ADB_BEGINNING;
    struct dirent *de;
    adb_time bounds[3];
    
    DIR *dir = opendir(path);
    if (!dir) return ADB_INVALTIME;
    while ((de = readdir(dir))) {
        // Only interested in blocks
        if (de->d_type == DT_REG && sscanf(de->d_name, FMT_TS, &bts)) {
            // Update the highest lower bound
            if (bts > hlb) hlb = bts;
            if (bts > ts) break;
        }
    }
    closedir(dir);
    
    // Read the clean manifest
    int ret = recall(path, CLEAN, bounds, sizeof(bounds));
    if (ret < 0) {
        return hlb;
    }
    
    // Wait if we land inside of the range
    if (bounds[0] <= hlb && hlb <= bounds[1]) {
        wait_on_clean(path);
        return bounds[2];
    }
    
    return hlb;
}


adb_time
var_blockbelow(const char *path, adb_time ts)
{
    adb_time bts, hlb = ADB_BEGINNING;
    struct dirent *de;
    adb_time bounds[3];
    
    DIR *dir = opendir(path);
    if (!dir) return ADB_INVALTIME;
    while ((de = readdir(dir))) {
        // Only interested in blocks
        if (de->d_type == DT_REG && sscanf(de->d_name, FMT_TS, &bts)) {
            // Update the highest lower bound
            if (bts > hlb) hlb = bts;
            if (bts > ts) break;
        }
    }
    closedir(dir);
    
    // Read the clean manifest
    int ret = recall(path, CLEAN, bounds, sizeof(bounds));
    if (ret < 0) {
        return hlb;
    }
    
    // Wait only if outside of the range and request to go inside
    if (ts > bounds[1] && hlb <= bounds[1]) {
        wait_on_clean(path);
        return bounds[2];
    }
    
    return hlb;
}

adb_time
var_blockabove(const char *path, adb_time ts)
{
    adb_time bts, lub = ADB_ENDOFTIME;
    struct dirent *de;
    adb_time bounds[3];
    
    DIR *dir = opendir(path);
    if (!dir) return ADB_INVALTIME;
    while ((de = readdir(dir))) {
        // Only interested in blocks
        if (de->d_type == DT_REG && sscanf(de->d_name, FMT_TS, &bts)) {
            // Update the lowest upper bound
            if (bts < lub) lub = bts;
            if (bts > ts) break;
        }
    }
    closedir(dir);
    
    // Read the clean manifest
    int ret = recall(path, CLEAN, bounds, sizeof(bounds));
    if (ret < 0) {
        return lub;
    }
    
    // Wait only if outside of the range and request to go inside
    if (ts < bounds[0] && lub >= bounds[0]) {
        wait_on_clean(path);
        return bounds[2];
    }
    
    return lub;
}

int
var_blockrange(const char *path, adb_time from, adb_time to,
               adb_time *blocks, size_t n)
{
    int i = 0;
    adb_time ts;
    struct dirent *de;
    
    DIR *dir = opendir(path);
    if (!dir) return -1;
    while ((de = readdir(dir)) && i < n) {
        // Only interested in blocks
        if (de->d_type == DT_REG && sscanf(de->d_name, FMT_TS, &ts)) {
            if (from <= ts && ts <= to) {
                // Add to blocks
                blocks[i++] = ts;
            }
        }
    }
    if (closedir(dir) < 0) return -1; 
    
    qsort(blocks, i, sizeof(adb_time), cmp_time);
    return i;
}
