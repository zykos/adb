/*******************************************************************************
 adb.h
 adb
 
 Copyright (c) 2012, Michael Sokolnicki
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 
 
 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef ADB_ADB_H
#define ADB_ADB_H


#include <stdint.h>     /* for int64_t */
#include <stddef.h>     /* for size_t */


// Relative time construction macros
#define ADB_US          (1)
#define ADB_MS          (ADB_US * 1E3)
#define ADB_SEC         (ADB_MS * 1E3)
#define ADB_MIN         (ADB_SEC * 60)
#define ADB_HOUR        (ADB_MIN * 60)
#define ADB_DAY         (ADB_HOUR* 24)
#define ADB_WEEK        (ADB_DAY  * 7)
#define ADB_INVALTIME   (INT64_MIN)
#define ADB_BEGINNING   (INT64_MIN + 1)
#define ADB_ENDOFTIME   (INT64_MAX)

// Time conversion macros
#define ADB_TV2TS(tv)   (tv.tv_sec*1000000 + tv.tv_usec)
#define ADB_TS2TV(ts)   ((struct timeval){ts/1000000, ts%1000000})

// Variable parameter macros
#define ADB_OLDBLOCK    (0)
#define ADB_NEWBLOCK    (1)

#define ADB_STAMPED     (0)
#define ADB_SLOTTED(g)  (g)
#define ADB_CSTRING     (-1)

// Type definitions
typedef int64_t     adb_time;
typedef int64_t     adb_store;
typedef enum mode   adb_mode;
typedef struct type adb_type;
typedef struct var  adb_var;

// Operation mode
enum mode {
    ADB_WRMODE = 0,
    ADB_RDMODE = 1,
    ADB_CLMODE = 2
};

// Variable type information
struct type {
    adb_store   storage;
    size_t      size;
    char        info[25];
};

// Variable manipulation handle
struct var {
    const char  *path;
    adb_mode    opmode;
    adb_time    start;
    adb_type    type;
    struct ci   *clnnfo;    // reserved
    struct rb   *rblock;    // reserved
    struct wb   *wblock;    // reserved
};


#ifdef __cplusplus
extern "C" {
#endif

/// Get an instant value corresponding to the current time
/**
 *  This function relies on the gettimeofday unix call, which returns the system
 *  time as both POSIX time and its sub-second (to a precision of a microsecond)
 *  component. On modern POSIX systems, this corresponds exactly to UTC time. In
 *  older systems, or system with no support for UTC, this may cause
 *  interoperability issues and should be manually corrected.
 *
 *  \return an adb_ts value
 *  \sa adb_push, adb_get
 */
adb_time
adb_now(void);

/// Open a variable and return an variable handle
/**
 *  The variable can be opened in three modes:
 *      - Write mode (ADB_WRITE)
 *      - Read mode (ADB_READ)
 *      - Clean mode (ADB_CLEAN)
 *  The behavior and availability of operations depend on the mode. All handles
 *  must be closed after usage to guarentee any changes are consolidated.
 *
 *  In write mode, the variable behaves as a stack. The cursor is always placed
 *  at the last valid event. If start is set to 0, the most recent record file 
 *  is used, otherwise a new file starting at start is created. The available
 *  operations are adb_get, adb_insert and adb_remove. When the handle is 
 *  closed, all operations are committed to disk.
 *
 *  In read mode, the variable behaves as a read-only doubly-linked list.
 *  The cursor is placed at the first instant whose value is greater or equal to
 *  the given start value. The available operations are adb_get, adb_prev and
 *  adb_next. All the operations in retreive mode are non-destructive, but
 *  closing the handle will allow other destructive operations to proceed.
 *
 *  In clean mode, the variable behaves as a singly-linked list that can be
 *  pruned. The cursor is placed at the first instant whose value is greater or
 *  equal to the given start value. The available operations in this mode are
 *  adb_get, adb_prev and adb_keep. Iteration can only happen towards the
 *  future, and when the handle is closed, only the events for which adb_keep
 *  was invoked will remain.
 *
 *  \param var the variable handle containing the parameters of the operation
 *  \param timeout the timeout in microseconds
 *  \return 0 on success, a negative number on failure
 */
int
adb_open(adb_var *var);

/// Close an variable handle
/**
 *  Free all memory claimed by the handle, unlock and close any open files and
 *  flush any destructive operations to disk.
 *
 *  \param var the variable handle to close
 *  \return 0 on success, a negative number on failure
 */
int
adb_close(adb_var *var);

/// Get the timestamp at the cursor
/**
 *  \param var the variable handle
 *  \return the time of the event under the cursor on success, 0 on failure
 */
adb_time
adb_key(adb_var *var);

/// Get the value of the payload at the cursor
/**
 *  \param var the variable handle
 *  \return a pointer to the start of the value on success, 0 on failure
 */
const void *
adb_value(adb_var *var);

/// Insert a new event after the cursor
/**
 *  Nbytes specifies the size of the buffer. If the variable's size is smaller
 *  than nbytes, the buffer will be written up to that size. If it is bigger,
 *  the rest of the space will be set to 0.
 *
 *  For variables using slotted storage, the key is ignored for all but the very
 *  first insert operation.
 *
 *  This function can only be called on write handles.
 *  \param var the variable handle
 *  \param key an adb_ts, containing the instant
 *  \param value a pointer to a buffer containing the value
 *  \return 0 on success, a negative number on failure
 */
int
adb_insert(adb_var *var, adb_time key, const void *value);

/// Remove the event at the cursor
/**
 *  This function can only be called on write and clean handles.
 *  \param var the variable handle
 *  \return 0 on success, a negative number on failure
 */
int
adb_remove(adb_var *var);

/// Move the cursor to the previous event
/**
 *  This function can only be called on read handles.
 *  \param var the variable handle
 *  \return 0 on success, a negative number on failure
 */
int
adb_prev(adb_var *var);

/// Move the cursor to the next event
/**
 *  This function can only be called on read and clean handles.
 *  \param var the variable handle
 *  \return 0 on success, a negative number on failure
 */
int
adb_next(adb_var *var);

#ifdef __cplusplus
}
#endif

#endif /* ADB_ADB_H */
