include_directories(${ADB_SOURCE_DIR}/src)
link_directories(${ADB_BINARY_DIR}/src)

# Build ADB library
add_library(adb STATIC adb.c block.c var.c)
