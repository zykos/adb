/*******************************************************************************
 block.c
 adb
 
 Copyright (c) 2012, Michael Sokolnicki
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 
 
 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include "block.h"

#include <errno.h>      /* errno and errors */
#include <fcntl.h>      /* open */
#include <stdlib.h>     /* malloc and free */
#include <stdio.h>      /* snprintf */
#include <string.h>     /* memset */
#include <sys/mman.h>   /* mmap */
#include <sys/stat.h>   /* permission flags on linux */
#include <unistd.h>     /* close */

#include "var.h"


// Main functions

rblock *
block_ropen(const char *path, adb_time base)
{
    int ret;
    rblock *blk;
    char datapath[PATH_MAX], keypath[PATH_MAX];
    
    // Generate paths
    ret = DATFILE(path, base, datapath);
    if (ret < 0) return NULL;
    ret = AUXFILE(path, base, keypath);
    if (ret < 0) return NULL;
    
    // Allocate rblock
    blk = malloc(sizeof(rblock));
    if (!blk) return NULL;
    memset(blk, 0, sizeof(rblock));
    blk->base = base;
    
    // Open data file
    blk->datafd = open(datapath, O_RDONLY);
    if (!blk->datafd) goto error;
    ret = flock(blk->datafd, LOCK_SH);
    if (ret < 0) goto error;
    
     // Memmap data file
    blk->datalen = lseek(blk->datafd, 0, SEEK_END);
    blk->datamap = mmap(0, blk->datalen, PROT_READ, MAP_SHARED, blk->datafd, 0);
    if (blk->datamap == MAP_FAILED) goto error;
    memcpy(&blk->h, blk->datamap, sizeof(blk->h));
    
    // Storage specific actions
    if (blk->h.storage == ADB_CSTRING) {
        /* TODO: unimplemented */
        errno = EINVAL;
        return NULL;
    } else {
        // Both stamped and slotted have fixed record length
        blk->elms = (blk->datalen - sizeof(blk->h)) / blk->h.size;
    }
    
    // Storage specific actions
    if (blk->h.storage == ADB_STAMPED) {
        // Open key file
        blk->keyfd = open(keypath, O_RDONLY);
        if (!blk->keyfd) goto error;
        ret = flock(blk->keyfd, LOCK_SH);
        if (ret < 0) goto error;
        
        // Memmap key file
        blk->keys = mmap(NULL, blk->elms * sizeof(adb_time), PROT_READ,
                         MAP_SHARED, blk->keyfd, 0);
        if (blk->keys == MAP_FAILED) goto error;
    }

    return blk;
error:
    block_rclose(blk);
    return NULL;
}

void
block_rclose(rblock *blk)
{
    if (blk->datafd) {
        flock(blk->datafd, LOCK_UN);
        close(blk->datafd);
        blk->datafd = 0;
    }
    if (blk->keyfd) {
        flock(blk->datafd, LOCK_UN);
        close(blk->keyfd);
        blk->keyfd = 0;
    }
    if (!blk->datamap) {
        munmap(blk->datamap, blk->datalen);
        blk->datamap = 0;
    }
    if (!blk->keys) {
        munmap(blk->keys, blk->elms * sizeof(adb_time));
        blk->keys = 0;
    }
    free(blk);
}

char *
block_curdata(rblock *blk)
{
    if (blk->h.storage == ADB_CSTRING) {
        errno = EINVAL;
        return NULL;
    }
    return blk->datamap + sizeof(blk->h) + (blk->cur * blk->h.size);
}

adb_time
block_curkey(rblock *blk)
{
    switch (blk->h.storage) {
        case ADB_CSTRING:
            /* TODO: unimplemented */
            errno = EINVAL;
            return ADB_INVALTIME;
        case ADB_STAMPED:
            return blk->keys[blk->cur];
        default: /* ADB_SLOTTED */
            return blk->base + (blk->cur * blk->h.storage);
    }
}

int
block_moveto(rblock *blk, adb_time key)
{
    off_t bi, mi, ei;
    
    // Shortcut
    if (key == blk->base) return 0;
    
    switch (blk->h.storage) {
        case ADB_CSTRING:
            /* TODO: unimplemented */
            errno = EINVAL;
            return -1;
        case ADB_STAMPED:
            // Binary search for the highest lower bound of the time
            ei = blk->elms;
            bi = 0;
            do {
                mi = (ei + bi) / 2;
                if      (key < blk->keys[mi]) ei = mi;
                else if (key > blk->keys[mi]) bi = mi;
                else    { ei = mi; break; };
            } while (ei > bi);
            blk->cur = ei;
            return 0;
        default: /* ADB_SLOTTED */
            // Cursor position can be computed directly
            blk->cur = (key - blk->base) / blk->h.storage;
            // Stay within bounds;
            if (blk->cur > blk->elms-1) blk->cur = blk->elms-1;
            if (blk->cur < 0)           blk->cur = 0;
            return 0;
    }
}


/******************************************************************************/

wblock *
block_create(const char *path, adb_time base, adb_type *h)
{
    int ret;
    wblock *blk;
    char datapath[PATH_MAX], keypath[PATH_MAX];
    
    // Generate paths
    ret = DATFILE(path, base, datapath);
    if (ret < 0) return NULL;
    ret = AUXFILE(path, base, keypath);
    if (ret < 0) return NULL;
    
    // Allocate wblock
    blk = malloc(sizeof(wblock));
    if (!blk) return NULL;
    memset(blk, 0, sizeof(wblock));
    blk->h = *h;
    blk->base = base;
    blk->last = base-1; // to satisfy key > last on insertion
    
    // Open data file
    blk->datafs = fopen(datapath, "w");
    if (!blk->datafs) goto error;
    ret = flock(fileno(blk->datafs), LOCK_EX);
    if (ret < 0) goto error;
    
    // Write header
    ret = (int)fwrite(h, sizeof(*h), 1, blk->datafs);
    if (ret != 1) goto error;
    
    // Storage specific actions
    switch (blk->h.storage) {
        case ADB_CSTRING:
            /* TODO: unimplemented */
            errno = EINVAL;
            return NULL;
        case ADB_STAMPED:
            // Create key file
            blk->keyfs = fopen(keypath, "w");
            if (!blk->keyfs) goto error;
            ret = flock(fileno(blk->keyfs), LOCK_EX);
            if (ret < 0) goto error;
        default: /* ADB_SLOTTED */
            break;
    }
    
    return blk;
error:
    block_wclose(blk);
    return NULL;
}

wblock *
block_wopen(const char *path, adb_time base)
{
    int ret;
    wblock *blk;
    long bytes;
    char datapath[PATH_MAX], keypath[PATH_MAX];
    
    // Generate paths
    ret = DATFILE(path, base, datapath);
    if (ret < 0) return NULL;
    ret = AUXFILE(path, base, keypath);
    if (ret < 0) return NULL;
    
    // Allocate wblock
    blk = malloc(sizeof(wblock));
    if (!blk) return NULL;
    memset(blk, 0, sizeof(wblock));
    blk->base = base;
    
    // Open data file
    blk->datafs = fopen(datapath, "a+");
    if (!blk->datafs) goto error;
    ret = flock(fileno(blk->datafs), LOCK_EX);
    if (ret < 0) goto error;
    
    // Extract header
    fseek(blk->datafs, 0, SEEK_SET);
    ret = (int)fread(&blk->h, sizeof(blk->h), 1, blk->datafs);
    if (ret != 1) {
        int eof = feof(blk->datafs);
        int err = ferror(blk->datafs);
        printf("eof = %i, err = %i\n", eof, err);
        goto error;
    }
    
    // Storage specific actions
    switch (blk->h.storage) {
        case ADB_CSTRING:
            /* TODO: unimplemented */
            errno = EINVAL;
            return NULL;
        case ADB_STAMPED:
            // Open key file
            blk->keyfs = fopen(keypath, "a+");
            if (!blk->keyfs) goto error;
            ret = flock(fileno(blk->keyfs), LOCK_EX);
            if (ret < 0) goto error;
            // Read last insertion key
            ret = fseek(blk->keyfs, -sizeof(blk->last), SEEK_END);
            if (ret < 0  ) goto error;
            ret = (int)fread(&blk->last, sizeof(blk->last), 1, blk->keyfs);
            if (ret != 1) goto error;
            // Set file mode to write-only and append
            blk->keyfs = freopen(keypath, "a", blk->keyfs);
            if (!blk->keyfs) goto error;
            break;
        default: /* ADB_SLOTTED */
            // Compute last insertion key
            bytes = ftell(blk->datafs);
            blk->last = blk->base + (bytes / blk->h.size) * blk->h.storage;
            break;
    }
    
    // Set file mode to write-only and append
    blk->datafs = freopen(datapath, "a", blk->datafs);
    if (!blk->datafs) goto error;

    return blk;
error:
    block_wclose(blk);
    return NULL;
}

void
block_wclose(wblock *blk)
{
    if (blk->datafs) {
        fflush(blk->datafs);
        flock(fileno(blk->datafs), LOCK_UN);
        fclose(blk->datafs);
        blk->datafs = 0;
    }
    if (blk->keyfs) {
        fflush(blk->keyfs);
        flock(fileno(blk->keyfs), LOCK_UN);
        fclose(blk->keyfs);
        blk->keyfs = 0;
    }
    free(blk);
}

int
block_write(wblock *blk, adb_time key, const char *value)
{
    size_t ret;
    
    switch (blk->h.storage) {
        case ADB_CSTRING:
            /* TODO: unimplemented */
            errno = EINVAL;
            return -1;
        case ADB_STAMPED:
            // Check if timestamp is valid
            if (key < blk->last) {
                errno = EDOM;
                return 0;
            }
            // Write key
            ret = fwrite(&key, sizeof(key), 1, blk->keyfs);
            if (ret != 1) return -1;
            
            // Write data
            ret = fwrite(value, blk->h.size, 1, blk->datafs);
            if (ret != 1) {
                // Rollback key write
                ftruncate(fileno(blk->keyfs), ftell(blk->keyfs) - sizeof(key));
                return -1;
            }
            blk->last = key;
            break;
        default: /* ADB_SLOTTED */
            // Write data
            ret = fwrite(value, blk->h.size, 1, blk->datafs);
            if (ret != 1) return -1;
            
            // Compute inserted timestamp
            blk->last += blk->h.storage;
            break;
    }
    return 0;
}

int
block_remove(wblock *blk)
{
    int ret;
    
    switch (blk->h.storage) {
        case ADB_CSTRING:
            /* TODO: unimplemented */
            errno = EINVAL;
            return -1;
        case ADB_STAMPED:
            // Rollback on key entry
            if (!blk->keyfs) {
                errno = EINVAL;
                return -1;
            }
            ret = ftruncate(fileno(blk->keyfs),
                            ftell(blk->keyfs) - sizeof(adb_time));
        default: /* ADB_SLOTTED */
            // Rollback one data entry
            if (!blk->datafs) {
                errno = EINVAL;
                return -1;
            }
            ret = ftruncate(fileno(blk->datafs),
                            ftell(blk->datafs) - blk->h.size);
            return ret;
    }    
    return -1;
}
