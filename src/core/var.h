/*******************************************************************************
 var.h
 adb
 
 Copyright (c) 2012, Michael Sokolnicki
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 
 
 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#ifndef ADB_VAR_H
#define ADB_VAR_H

#include "adb.h"

#if defined(__APPLE__) || defined (BSD)
#define FMT_TS  "%016llx"
#elif defined(__linux__)
#define FMT_TS  "%016lx"
#endif
#define TS_LEN  16
#define PATH_MAX 1024

#define DATFILE(src, ts, dst) snprintf(dst, PATH_MAX, "%s/" FMT_TS, src, ts);
#define AUXFILE(src, ts, dst) snprintf(dst, PATH_MAX, "%s/." FMT_TS, src, ts);
#define CLEANPATH(src, dst) snprintf(dst, PATH_MAX, "%s/.cln", src)
#define TEMP_PATH(src, dst) snprintf(dst, PATH_MAX, "%s/.tmp", src)


int
var_cleanup(const char *path, adb_time write_block, adb_time *read_blocks, size_t n);

int
var_recover(const char *path);

adb_time
var_blockat(const char *path, adb_time ts);

adb_time
var_blockbelow(const char *path, adb_time ts);

adb_time
var_blockabove(const char *path, adb_time ts);

int
var_blockrange(const char *path, adb_time from, adb_time to,
               adb_time *blocks, size_t n);

#endif
