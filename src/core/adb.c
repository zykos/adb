/*******************************************************************************
 adb.c
 adb
 
 Copyright (c) 2012, Michael Sokolnicki
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 
 
 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include "adb.h"

#include <errno.h>      /* errno and errors */
#include <stdlib.h>     /* malloc and free */
#include <stdio.h>      /* snprintf */
#include <string.h>     /* bzero and memcpy */
#include <sys/time.h>   /* struct timeval and gettimeofday */

#include "block.h"
#include "var.h"


struct ci {
    int count;
    adb_time first;
};

// Helper functions

static int
skip_next(adb_var *var)
{
    adb_time base;
    
    if (block_atend(var->rblock)) {
        // Find the next block, if it exists
        base = var_blockabove(var->path, var->rblock->base);
        if (base == ADB_INVALTIME) return -1;
        
        // If there are no more blocks, return 1
        if (base == var->rblock->base) return 1;
        
        // Open the new block
        rblock *blk = block_ropen(var->path, base);
        if (!blk) return -1;
        
        // Count the opened blocks in clean mode
        if (var->opmode == ADB_CLMODE) var->clnnfo->count++;
        
        // Close the old one, replace with the new
        block_rclose(var->rblock);
        var->rblock = blk;
    } else {
        // Just move to the next/previous record inside the set
        block_movefwd(var->rblock);
    }
    return 0;
}

static int
skip_prev(adb_var *var)
{
    adb_time base;
    
    if (block_atbeg(var->rblock)) {
        // Find the next block, if it exists
        base = var_blockbelow(var->path, var->rblock->base);
        if (base == ADB_INVALTIME) return -1;
        
        // If there are no more blocks, return 1
        if (base == var->rblock->base) return 1;
        
        // Open the new block
        rblock *blk = block_ropen(var->path, base);
        if (!blk) return -1;
        
        // Close the old one, replace with the new
        block_rclose(var->rblock);
        var->rblock = blk;
    } else {
        // Just move to the next/previous record inside the set
        block_movebwd(var->rblock);
    }
    return 0;
}


// API functions

adb_time
adb_now(void) {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return ADB_TV2TS(tv);
}


int
adb_open(adb_var *var)
{
    int ret;
    adb_time base;
    
    // Recover from potential failure
    //ret = var_recover(var->path);
    //if (ret < 0) goto error;
    
    // Copy the remaining values
    var->clnnfo = NULL;
    var->rblock = NULL;
    var->wblock = NULL;
    
    switch (var->opmode) {
        case ADB_WRMODE:
            // If start is ADB_OLDBLOCK, reopen the last block
            // Otherwise, the block will be created on insert
            if (var->start == ADB_OLDBLOCK) {
                base = var_blockat(var->path, ADB_ENDOFTIME);
                if (base == ADB_INVALTIME) goto error;
                if (base != ADB_BEGINNING) {                
                    var->wblock = block_wopen(var->path, base);
                    if (!var->wblock) goto error;
                }
            }
            break;
        
        case ADB_CLMODE:
            var->clnnfo = malloc(sizeof(*var->clnnfo));
            var->clnnfo->count = 1;
            var->clnnfo->first = base;
        case ADB_RDMODE:
            // Find corresponding block
            base = var_blockat(var->path, var->start);
            if (base == ADB_INVALTIME) goto error;
            
            // Attempt to open
            var->rblock = block_ropen(var->path, base);
            if (!var->rblock) goto error;
            
            // Move pointer to the starting record
            ret = block_moveto(var->rblock, var->start);
            if (ret < 0) goto error;
            
            break;
        default:
            errno = EINVAL;
            return -1;
    }
    
    return 0;
error:
    if (var->clnnfo) free(var->clnnfo);
    if (var->rblock) block_rclose(var->rblock);
    if (var->wblock) block_wclose(var->wblock);
    return -1;
}

int
adb_close(adb_var *var)
{
    int ret;
    
    if (var->opmode == ADB_CLMODE) {
        // Get base of the last read block and the write block
        adb_time read_base = ((rblock *)var->rblock)->base;
        adb_time write_base = ((wblock *)var->wblock)->base;
        
        // Get an array of all the affected blocks
        adb_time *blocks = malloc(sizeof(adb_time) * var->clnnfo->count);
        ret = var_blockrange(var->path, var->clnnfo->first, read_base,
                             blocks, var->clnnfo->count);
        if (ret < 0) {
            free(blocks);
            return ret;
        }
        
        // Perform the cleanup
        ret = var_cleanup(var->path, write_base, blocks, var->clnnfo->count);
        if (ret < 0) {
            free(blocks);
            return ret;
        }
        
        free(blocks);
    }
    
    // Close blocks if open
    if (var->rblock) block_rclose(var->rblock);
    if (var->wblock) block_wclose(var->wblock);
    return 0;
}

adb_time
adb_key(adb_var *var)
{
    if (!var->rblock) return 0;
    return block_curkey(var->rblock);
}

const void *
adb_value(adb_var *var)
{
    if (!var->rblock) return NULL;
    return block_curdata(var->rblock);
}

int
adb_insert(adb_var *var, adb_time key, const void *value)
{
    switch (var->opmode) {
        case ADB_WRMODE:
            // If block isn't opened, create one
            if (!var->wblock) {
                var->wblock = block_create(var->path, key, &var->type);
                if (!var->wblock) return -1;
            }
            
            // Write data
            return block_write(var->wblock, key, value);
        default:
            errno = EINVAL;
            return -1;
    }
}

int
adb_remove(adb_var *var)
{    
    switch (var->opmode) {
        case ADB_WRMODE:
            return block_remove(var->wblock);
        case ADB_CLMODE:
            // Skip to next entry
            return skip_next(var);
        default:
            errno = EINVAL;
            return -1;
    }
}

int
adb_prev(adb_var *var)
{
    switch (var->opmode) {
        case ADB_RDMODE:
            // Skip to previous entry
            return skip_prev(var);
        default:
            errno = EINVAL;
            return -1;
    }
}

int
adb_next(adb_var *var)
{
    adb_time key;

    switch (var->opmode) {
        case ADB_CLMODE:
            // Read record key
            key = block_curkey(var->rblock);
            
            // If block isn't opened, create one
            if (!var->wblock) {
                char cln_path[1024];
                int ret = CLEANPATH(var->path, cln_path);
                if (ret < 0) return 0;
                
                var->wblock = block_create(cln_path, key, &var->type);
                if (!var->wblock) return 0;
            }
            
            // Write back to the new block
            int ret = block_write(var->wblock, key, block_curdata(var->rblock));
            if (ret < 0) return 0;
        case ADB_RDMODE:
            // Skip to next entry
            return skip_next(var);
        default:
            errno = EINVAL;
            return -1;
    }
}


